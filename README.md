# Zig in Depth Video Course Code Repository

**NOTE: This series and code is only compatible with Zig version 0.11.0!**

This repository has the companion code shown throughout the 
[Zig in Depth video course](https://youtube.com/playlist?list=PLtB7CL7EG7pCw7Xy1SQC53Gl8pI7aDg9t&si=5O0zTlEyse6hMC0V).
