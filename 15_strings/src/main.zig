const std = @import("std");

pub fn main() !void {
    // A string literal is a const pointer to a sentinel terminated array.
    // The sentinel is 0 also known as the null character in C.
    // The array's bytes are included as part of the generated binary file.
    const hello = "hello";
    std.debug.print("type of \"hello\": {}\n", .{@TypeOf(hello)});

    std.debug.print("\n", .{});

    // When you need a more general type for strings in Zig, you'll use
    // a slice of bytes; either const `[]const u8` or not `[]u8`.
    printStrint("Hello world!");

    std.debug.print("\n", .{});

    // Zig has a minimal set of escapes.
    std.debug.print("escapes: \t \" \' \x65 \u{e9} \n \r", .{});

    std.debug.print("\n", .{});

    // Indexing into a string produces individual bytes.
    std.debug.print("hello[2] == {}, {0c}, {0u}\n", .{hello[2]});

    std.debug.print("\n", .{});

    // Zig source is UTF-8 encoded but strings can contain non-UTF-8 using \x syntax.
    // Note we can coerce a literal into a slice of bytes.
    var hello_acute: []const u8 = "h\xe9llo";
    std.debug.print("hello_acute: {s}, len: {}\n", .{ hello_acute, hello_acute.len });
    hello_acute = "h\u{e9}llo";
    std.debug.print("hello_acute: {s}, len: {}\n", .{ hello_acute, hello_acute.len });

    std.debug.print("\n", .{});

    // Multiline literals begin with `\\` and don't process any escapes.
    const multiline =
        \\ This is a multiline
        \\ string in Zig. This \n
        \\ will note be processed 
        \\ as a new line.
    ;
    std.debug.print("multiline: {s}\n", .{multiline});

    std.debug.print("\n", .{});

    // There are no characters in Zig. A Unicode code point literal is
    // specified withing single quotes.
    const ziguana = '🦎';
    // We usually use `u21` as the type for code points.
    const bolt: u21 = '⚡';
    std.debug.print("{u} Zig! {u}\n", .{ ziguana, bolt });

    std.debug.print("\n", .{});

    // std.unicode provides functions for handling Unicode text.
    std.debug.print("\"h\xe9llo\" is valid UTF-8? {}\n", .{std.unicode.utf8ValidateSlice("h\xe9llo")});
    // std.ascii provides functions for handling ASCII text.
    std.debug.print("'A' is uppercase? {}\n", .{std.ascii.isUpper('A')});

    // I developed Ziglyph, a library for processing Unicode text in Zig, and Zigstr,
    // an UTF-8 string type for Zig, but we can talk about them after we discuss
    // using dependencies in our projects.
}

fn printStrint(s: []const u8) void {
    std.debug.print("{s}\n", .{s});
}
