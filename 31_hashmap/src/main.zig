const std = @import("std");

// Allocates memory for the email field.
const User = struct {
    allocator: std.mem.Allocator,
    id: usize,
    email: []u8,

    fn init(
        allocator: std.mem.Allocator,
        id: usize,
        email: []const u8,
    ) !User {
        return .{
            .allocator = allocator,
            .id = id,
            .email = try allocator.dupe(u8, email),
        };
    }

    fn deinit(self: *User) void {
        self.allocator.free(self.email);
    }
};

// Uses a hash map internally to store Users mapped to their IDs.
const UserData = struct {
    map: std.AutoHashMap(usize, User),

    fn init(allocator: std.mem.Allocator) UserData {
        return .{ .map = std.AutoHashMap(usize, User).init(allocator) };
    }

    fn deinit(self: *UserData) void {
        self.map.deinit();
    }

    // Clobbers existing entry.
    fn put(self: *UserData, user: User) !void {
        try self.map.put(user.id, user);
    }

    fn get(self: UserData, id: usize) ?User {
        return self.map.get(id);
    }

    // Can be called for non-existent keys.
    fn del(self: *UserData, id: usize) ?User {
        return if (self.map.fetchRemove(id)) |kv| kv.value else null;
    }
};

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    var users = UserData.init(allocator);
    defer users.deinit();

    // Add some entries.
    var jeff = try User.init(allocator, 1, "jeff@foo.io");
    defer jeff.deinit();
    try users.put(jeff);

    var alice = try User.init(allocator, 2, "alice@foo.io");
    defer alice.deinit();
    try users.put(alice);

    var bob = try User.init(allocator, 3, "bob@foo.io");
    defer bob.deinit();
    try users.put(bob);

    // Get some entries.
    if (users.get(jeff.id)) |user| std.debug.print("got id: {}, email: {s}\n", .{ user.id, user.email });
    if (users.get(alice.id)) |user| std.debug.print("got id: {}, email: {s}\n", .{ user.id, user.email });
    if (users.get(bob.id)) |user| std.debug.print("got id: {}, email: {s}\n", .{ user.id, user.email });

    // Delete an entry.
    _ = users.del(bob.id);
    if (users.get(bob.id)) |user| std.debug.print("got id: {}, email: {s}\n", .{ user.id, user.email });

    // Entry count.
    std.debug.print("count: {}\n", .{users.map.count()});
    // Check if entry in map.
    std.debug.print("contains alice? {}\n", .{users.map.contains(alice.id)});

    // You can iterate over entries, keys, or values.
    var entry_iter = users.map.iterator();
    while (entry_iter.next()) |entry| std.debug.print("id: {}, email: {s}\n", .{ entry.key_ptr.*, entry.value_ptr.email });

    // Get an existing entry or add it if not there.
    var gopr = try users.map.getOrPut(bob.id);
    if (!gopr.found_existing) gopr.value_ptr.* = bob;
    // Verify it's there.
    std.debug.print("contains bob? {}\n", .{users.map.contains(bob.id)});
    if (users.get(bob.id)) |user| std.debug.print("got id: {}, email: {s}\n", .{ user.id, user.email });

    // If you need a set of unique items, you can create a
    // hash map with void value type.
    var primes = std.AutoHashMap(usize, void).init(allocator);
    defer primes.deinit();
    // Add some primes.
    try primes.put(5, {});
    try primes.put(7, {});
    try primes.put(7, {});
    try primes.put(5, {});

    std.debug.print("primes in map: {}\n", .{primes.count()});
    std.debug.print("primes contains 5? {}\n", .{primes.contains(5)});
}
