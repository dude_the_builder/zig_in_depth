const std = @import("std");

fn work(id: usize) void {
    std.time.sleep(1 * std.time.ns_per_s);
    std.debug.print("{} finished\n", .{id});
}

pub fn main() !void {
    const cpus = try std.Thread.getCpuCount();

    // No threading.
    // for (0..cpus) |i| {
    //     work(i);
    // }

    // Manual threading with join.
    // var handles: [8]std.Thread = undefined;
    //
    // for (0..cpus) |i| {
    //     handles[i] = try std.Thread.spawn(.{}, work, .{i});
    // }
    //
    // for (handles) |h| h.join();

    // Manual threading with detach.
    // for (0..cpus) |i| {
    //     var handle = try std.Thread.spawn(.{}, work, .{i});
    //     handle.detach();
    // }
    //
    // std.time.sleep(1001 * std.time.ns_per_ms);

    // Thread pool.
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    var pool: std.Thread.Pool = undefined;
    try pool.init(.{ .allocator = allocator });
    defer pool.deinit();

    for (0..cpus) |i| {
        try pool.spawn(work, .{i});
    }
}
