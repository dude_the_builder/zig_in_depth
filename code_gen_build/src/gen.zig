const std = @import("std");

pub fn main() !void {
    var args = std.process.args();
    _ = args.next();
    const filename = args.next().?;

    var file = try std.fs.cwd().createFile(filename, .{});
    defer file.close();
    var bw = std.io.bufferedWriter(file.writer());
    const writer = bw.writer();

    const content =
        \\ const std = @import("std");
        \\
        \\ pub fn main() void {
        \\   std.debug.print("Hello, world!\n", .{});
        \\ }
        \\
    ;

    try writer.writeAll(content);
    try bw.flush();
}
